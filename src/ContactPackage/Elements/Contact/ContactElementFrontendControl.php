<?php

namespace Packages\ContactPackage\Elements\Contact;

use movi\Application\UI\Form;
use Packages\CmsPackage\Content\ElementControl;
use Packages\ContactPackage\Elements\Contact\Forms\ContactFormFactory;

class ContactElementFrontendControl extends ElementControl
{

	/** @var \Packages\ContactPackage\Elements\Contact\Forms\ContactFormFactory */
	private $contactFormFactory;


	public function __construct(ContactFormFactory $contactFormFactory)
	{
		$this->contactFormFactory = $contactFormFactory;
	}


	protected function createComponentForm()
	{
		$factory = $this->contactFormFactory;
		$factory->setContact($this->element->contact);

		$form = $factory->create();

		$form->onSuccess[] = function() {
			$this->presenter->flashMessage('Ďakujeme za Vašu správu.');
			$this->presenter->redirect('this');
		};

		return $form;
	}

} 