<?php

namespace Packages\ContactPackage\Elements\Contact\Forms;

use movi\Application\UI\Form;
use Packages\ContactPackage\Model\Entities\Recipient;
use Packages\ContactPackage\Model\Facades\RecipientsFacade;
use movi\Forms\EntityFormFactory;
use Packages\RedactorPackage\Forms\Controls\RedactorControl;
use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;

class ContactSetupFormFactory extends EntityFormFactory
{

	/** @var RecipientsFacade */
	private $recipientsFacade;


	protected function configure(Form $form)
	{
		$form->elementPrototype->class[] = 'ajax';

		$form->addText('subject', 'Predmet')
			->setRequired();

		$form['content'] = (new RedactorControl('Obsah'))
			->setRequired();

		$recipients = $form->addDynamic('recipients', function(Container $container) use ($form) {
				$container->addHidden('id');

				$container->addText('recipient', 'Adresát')
					->setRequired()
					->addRule($form::EMAIL);

				$remove = $container->addSubmit('remove', 'Odstrániť adresáta')
					->setValidationScope(FALSE);
				$remove->controlPrototype->class('btn-danger');
				$remove->onClick[] = function(SubmitButton $button) {
					$id = $button->parent->values->id;
					$recipient = $this->recipientsFacade->findOne($id);

					if ($recipient) {
						$this->recipientsFacade->delete($recipient);
					}
				};
			}, ($this->entity->isDetached() ? 1 : NULL));

		$add = $recipients->addSubmit('add', 'Pridať adresáta');
		$add->onClick[] = function(SubmitButton $button) {
			$button->parent->createOne();
		};
		$add->controlPrototype->class('btn-success');

		$form->addSubmit('save', 'Uložiť');
	}


	public function postProcessForm(Form $form)
	{
		if ($form['save']->isSubmittedBy()) {
			$recipients = $form->values->recipients;

			foreach ($this->entity->recipients as $recipient)
			{
				$this->recipientsFacade->delete($recipient);
			}

			foreach ($recipients as $value)
			{
				$recipient = new Recipient();
				$recipient->email = $value->recipient;
				$recipient->contact = $this->entity;

				$this->recipientsFacade->persist($recipient);
			}
		}
	}


	public function loadValues(Form $form)
	{
		parent::loadValues($form);

		if (!$this->entity->isDetached()) {
			$recipients = array();

			foreach ($this->entity->recipients as $recipient)
			{
				$recipients[] = array('id' => $recipient->id, 'recipient' => $recipient->email);
			}

			$form['recipients']->setDefaults($recipients);
		}
	}


	public function setRecipientsFacade(RecipientsFacade $recipientsFacade)
	{
		$this->recipientsFacade = $recipientsFacade;
	}

} 