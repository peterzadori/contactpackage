<?php

namespace Packages\ContactPackage\Elements\Contact;

use movi\Application\UI\Form;
use Packages\CmsPackage\Content\ElementControl;
use Packages\ContactPackage\Elements\Contact\Forms\ContactSetupFormFactory;
use Packages\ContactPackage\Model\Entities\Contact;
use Packages\ContactPackage\Model\Facades\ContactsFacade;

class ContactElementBackendControl extends ElementControl
{

	/** @var \Packages\ContactPackage\Elements\Contact\Forms\ContactSetupFormFactory */
	private $contactSetupFormFactory;

	/** @var \Packages\ContactPackage\Model\Facades\ContactsFacade */
	private $contactsFacade;

	/** @var Contact */
	private $contact;


	public function __construct(ContactSetupFormFactory $contactSetupFormFactory, ContactsFacade $contactsFacade)
	{
		$this->contactSetupFormFactory = $contactSetupFormFactory;
		$this->contactsFacade = $contactsFacade;
	}


	public function loadState(array $params)
	{
		parent::loadState($params);

		if ($this->element->contact === NULL) {
			$contact = new Contact();
			$contact->element = $this->element;

			$this->contactsFacade->persist($contact);

			$this->contact = $contact;
		} else {
			$this->contact = $this->element->contact;
		}
	}


	public function attached($presenter)
	{
		parent::attached($presenter);

		if ($this->isAjax()) $this->redrawControl();
	}


	protected function createComponentForm()
	{
		$form = $this->contactSetupFormFactory->create($this->contact);

		$form->onSuccess[] = function(Form $form) {
			if ($form['save']->isSubmittedBy()) $this->presenter->flashMessage('Údaje boli uložené.');
		};

		return $form;
	}

} 