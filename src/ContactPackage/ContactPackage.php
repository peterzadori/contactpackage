<?php

namespace Packages\ContactPackage;

use movi\DI\CompilerExtension;
use movi\Packages\IPackage;
use movi\Packages\Providers\IConfigProvider;

class ContactPackage extends CompilerExtension implements IPackage, IConfigProvider
{

	public function getConfigFiles()
	{
		return array(
			__DIR__ . '/Resources/config/package.neon'
		);
	}

} 