<?php

namespace Packages\ContactPackage\Elements\Contact;

interface IContactElementFrontendControlFactory
{

	/** @return ContactElementFrontendControl */
	public function create();

} 