<?php

namespace Packages\ContactPackage\Elements\Contact\Forms;

use Latte\Loaders\StringLoader;
use movi\Application\UI\Form;
use Packages\AppPackage\Settings\Settings;
use Packages\ContactPackage\Model\Entities\Contact;
use movi\Forms\FormFactory;
use Nette\InvalidStateException;
use Nette\Mail\IMailer;
use Nette\Mail\Message;

class ContactFormFactory extends FormFactory
{

	/** @var \Nette\Mail\IMailer */
	private $mailer;

	private $settings;

	/** @var Contact */
	private $contact;


	public function __construct(IMailer $mailer, Settings $settings)
	{
		$this->mailer = $mailer;
		$this->settings = $settings;
	}


	public function setContact(Contact $contact = NULL)
	{
		$this->contact = $contact;
	}


	protected function configure(Form $form)
	{
		$form->addText('name', 'Name');

		$form->addText('email', 'E-mail')
            ->addRule($form::FILLED)
			->addCondition($form::FILLED)
			->addRule($form::EMAIL);

		$form->addText('phone', 'Phone');

		$form->addTextArea('message', 'Message')
			->setRequired();

		$form->addSubmit('send', 'Send');
	}


	public function processForm(Form $form)
	{
        if ($this->contact === NULL) return false;

		$values = $form->values;

		$template = clone $form->presenter->template;

		$latte = $template->getLatte();
		$latte->setLoader(new StringLoader());

		$template->email = $values->email;
		$template->name = $values->name;
		$template->message = $values->message;
		$template->phone = $values->phone;

		$template->setFile($this->contact->content);

		foreach ($this->contact->recipients as $recipient)
		{
			$message = new Message();
			$message->setFrom($values->email, $values->name);
			$message->addTo($recipient->email);
			$message->setSubject($this->contact->subject);

			$message->setHtmlBody((string) $template);

			try {
				@$this->mailer->send($message);
			} catch (InvalidStateException $e) {

			}
		}
	}

}