<?php

namespace Packages\ContactPackage\Elements\Contact;

use Packages\CmsPackage\Model\Entities\Element;
use Packages\ContactPackage\Model\Entities\Contact;

/**
 * Class ContactElementEntity
 * @package Packages\ContactPackage\Elements\Contact
 *
 * @property Contact|NULL $contact m:belongsToOne(element_id)
 */
class ContactElementEntity extends Element
{

} 