<?php

namespace Packages\ContactPackage\Model\Entities;

use movi\Model\Entities\IdentifiedEntity;

/**
 * Class Recipient
 * @package Packages\ContactPackage\Model\Entities
 *
 * @property string $email m:size(128)
 *
 * @property Contact $contact m:hasOne
 */
final class Recipient extends IdentifiedEntity
{

} 