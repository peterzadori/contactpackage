<?php

namespace Packages\ContactPackage\Model\Entities;

use Packages\CmsPackage\Model\Entities\Element;
use movi\Model\Entities\IdentifiedEntity;

/**
 * Class Contact
 * @package Packages\ContactPackage\Model\Entities
 *
 * @property string $subject m:size(128)
 * @property string $content
 *
 * @property Element $element m:hasOne
 * @property Recipient[] $recipients m:belongsToMany
 */
class Contact extends IdentifiedEntity
{

} 