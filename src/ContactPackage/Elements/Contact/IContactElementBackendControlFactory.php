<?php

namespace Packages\ContactPackage\Elements\Contact;

interface IContactElementBackendControlFactory
{

	/** @return ContactElementBackendControl */
	public function create();

} 