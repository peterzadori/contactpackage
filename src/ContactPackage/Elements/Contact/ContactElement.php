<?php

namespace Packages\ContactPackage\Elements\Contact;

use Packages\CmsPackage\Content\ElementType;

class ContactElement extends ElementType
{

	/** @var IContactElementFrontendControlFactory */
	private $frontendControlFactory;

	/** @var IContactElementBackendControlFactory */
	private $backendControlFactory;


	public function __construct(IContactElementFrontendControlFactory $frontendControlFactory, IContactElementBackendControlFactory $backendControlFactory)
	{
		$this->frontendControlFactory = $frontendControlFactory;
		$this->backendControlFactory = $backendControlFactory;
	}


	public function getKey()
	{
		return 'contact';
	}


	public function getLabel()
	{
		return 'Kontaktný formulár';
	}


	public function getEntity()
	{
		return new ContactElementEntity();
	}


	public function getFrontendControl()
	{
		return $this->frontendControlFactory->create();
	}


	public function getBackendControl()
	{
		return $this->backendControlFactory->create();
	}


	public function getIcon()
	{
		return 'fa-envelope';
	}

} 